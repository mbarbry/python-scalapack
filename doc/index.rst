.. PyScalapack documentation master file, created by
   sphinx-quickstart on Sun Oct  8 11:10:08 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PyScalapack's documentation!
=======================================

PyScalapack is a Python_ wrapper to ScaLPACK library. It aims is to
provide a easy way to use ScaLAPACK_ routines in a Python_ program in the
same fashion than Scipy_ wrappers to BLAS_ and LAPACK_ libraries.

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   installation.rst

.. _MPI: http://www.mpi-forum.org/
.. _Netlib: http://www.netlib.org/scalapack/
.. _MKL: https://software.intel.com/en-us/mkl
.. _Python: http://www.python.org/
.. _BLACS: http://www.netlib.org/blacs/
.. _BLAS: http://www.netlib.org/blas/
.. _BLACS: http://www.netlib.org/blacs/
.. _LAPACK: http://www.netlib.org/lapack/
.. _ScaLAPACK: http://www.netlib.org/scalapack/
.. _mpi4py: https://pythonhosted.org/mpi4py/
.. _Scipy: https://www.scipy.org/
.. _repository: https://gitlab.com/mbarbry/python-scalapack


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
