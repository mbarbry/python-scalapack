Installation instructions
=========================

To install PyScalapack you will need:

* Python_ 2.7-3.5
* MPI_
* BLACS_
* a ScaLAPACK librairy (Netlib_, MKL_, ...)
* mpi4py_ package


To install the package follow the following instructions::

  $ git clone https://gitlab.com/mbarbry/python-scalapack.git
  $ cd python-scalapack/pyscalapack
  $ cp customize.py.arch.your_arch customize.py
  $ cd ..
  $ python setup.py install

You will probably need to edit the customize file to satisfy your need.
The customize.py file contains two dictionary, the first one contains info
about the libraries and include paths. The second contains informations
about C and Fortran compilers to use.

Check that you can import the library::
  
  $ python3 -c 'import pyscalapack'




.. _MPI: http://www.mpi-forum.org/
.. _Netlib: http://www.netlib.org/scalapack/
.. _MKL: https://software.intel.com/en-us/mkl
.. _Python: http://www.python.org/
.. _BLACS: http://www.netlib.org/blacs/
.. _mpi4py: https://pythonhosted.org/mpi4py/
.. _repository: https://gitlab.com/mbarbry/python-scalapack
