"""
    generate documentation of PyScalapack using the pdoc module.
    just run
    python generate_doc.py --type <doc type>
"""

import argparse
import subprocess
try:
    import pdoc
except:
    mesg = """
           pdoc must be install to generate documentation.
           pip install pdoc
           """
    raise SystemError(mesg)

parser = argparse.ArgumentParser()
parser.add_argument('--type', type=str, default='html', help="type of doc to generate")

args_par = parser.parse_args()
subprocess.call("pdoc --" + args_par.type + " PyScalapack", shell=True)
