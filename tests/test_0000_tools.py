import unittest
from mpi4py import MPI
import numpy as np
import pyscalapack.pyscalapack as pys
import sys


class KnowValues(unittest.TestCase):

    def test_pnum_pcoord(self):
        comm = MPI.COMM_WORLD
        rank = comm.Get_rank()

        iam, nprocs = pys.blacs_pinfo()
        context = pys.blacs_get(-1)

        nprow = int(np.sqrt(1.0*nprocs))
        npcol = int(nprocs/nprow)

        pys.blacs_gridinit(context, nprow, npcol)
        nprow, npcol, myrow, mycol = pys.blacs_gridinfo(context)

        if iam == 0:
            for i in range(nprocs):
                row_coord, col_coord = pys.blacs_pcoord(context, i)
                print("rank {0}: ({1}, {2})".format(i, row_coord, col_coord))

            for r in range(nprow):
                for c in range(npcol):
                    pnum = pys.blacs_pnum(context, r, c)
                    print("grid: ({0}, {1}) => proc: {2}".format(r, c, pnum))

        # pys.blacs_exit()

if __name__ == "__main__":
    print("Testing blacs tools")
    unittest.main()
