"""
PyScalapack: Python wrapper for ScaLapack routines
==================================================

Documentation is available in the docstrings and
online at http://mbarbry.pagekite.me/PyScalapack.

Contents
--------
Python wrapper for scalpack routines implemented:
    * blacs_pinfo
    * blacs_setup
    * blacs_get
    * blacs_exit
    * blacs_barrier
    * blacs_gridinfo
    * blacs_gridinit
    * blacs_gridmap
    * blacs_gridexit
    * pselset
    * pdelset
    * pcelset
    * pzelset
    * psgemv
    * pdgemv
    * psgemm
    * pdgemm
    * pssygvx
    * pdsygvx
    * numroc
    * descinit


Utility tools
-------------
::

 __version__       --- PyScalapack version string

"""
from __future__ import division, print_function, absolute_import

__all__ = []

# We first need to detect if we're being called as part of the scipy
# setup procedure itself in a reliable manner.
try:
    __PYSCALAPACK_SETUP__
except NameError:
    __PYSCALAPACK_SETUP__ = False


if __PYSCALAPACK_SETUP__:
    import sys as _sys
    _sys.stderr.write('Running from pyscalapack source directory.\n')
    del _sys
else:
    from pyscalapack.version import version as __version__
