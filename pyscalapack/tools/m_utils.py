from __future__ import division
import numpy as np
import pyscalapack.pyscalapack as pys

def l2g(il, p, n, np, nb):
    """
        convert local index to global index in block-cyclic distribution

        Input Parameters:
        -----------------
            il (int) local array index
            p  (int) processor array index
            n  (int) global array dimension
            np (int) processor array dimension
            nb (int) block size (Warning: must be 1!!)

        Output Parameters:
        ------------------
            i  (int) global array index
    """
    
    if nb != 1:
        raise ValueError("index convertor works only for nb =1 at the moment!")

    return int((((il/nb) * np) + p)*nb + il%nb)

def g2l(i,n,np,nb):
    """
        convert global index to local index in block-cyclic distribution

        Input Parameters:
        -----------------
            i  (int) global array index
            n  (int) global array dimension
            np (int) processor array dimension
            nb (int) block size (Warning:  must be 1!!)

        Output Parameters:
        ------------------
            p  (int) processor array index
            il (int) local array index
    """

    if nb != 1:
        raise ValueError("index convertor works only for nb =1 at the moment!")

    return int((i/nb)%np), int((i/(np*nb))*nb + i%nb)

def scatter_vector(full, nprow, row, nb, myrows, order="C"):
    """
        Scatter a vector over the nodes using the cyclic scalapack method
        Input Parameters:
        -----------------
            full   (1d array): the vectorto be scattered
            nprow  (int): number of row on the thread grid
            row    (int): row index of the thread on the thread grid
            nb     (int): number of block
            myrows (int): number of row of the scattered matrix
            order  (str, optional): order of the output matrix,
                                    * "C" row major
                                    * "F" column major

        Output Parameters:
        ------------------
            scatter (1d array): scattered matrix
    """

    scatter = np.zeros((myrows, 1), dtype=full.dtype, order=order)

    for i in range(full.shape[0]):
        iproc, myi = g2l(i, full.shape[0], nprow, nb)
        if row == iproc:
            scatter[myi, 0] = full[i]

    return scatter

def scatter_matrix(full, nprow, npcol, row, col, nb, myrows, mycols, order="C"):
    """
        Scatter a matrix over the nodes using the cyclic scalapack method
        Input Parameters:
        -----------------
            full   (2d array): the matrix to be scattered, ideally present only on the master node
            nprow  (int): number of row on the thread grid
            npcol  (int): number of column of the thread grid
            row    (int): row index of the thread on the thread grid
            col    (int): column index of the thread on the thread grid
            nb     (int): number of block
            myrows (int): number of row of the scattered matrix
            mycols (int): number of column of the scattered matrix
            order  (str, optional): order of the output matrix,
                                    * "C" row major
                                    * "F" column major

        Output Parameters:
        ------------------
            scatter (2d array): scattered matrix

        Example:
        --------

        import numpy as np
        import pyscalapack.pyscalapack as pys
        from pyscalapack.tools.m_utils import scatter_matrix

        # input
        N = 10
        A = np.random.randn(N, N)

        # Initialize the grid
        iam, nprocs = pys.blacs_pinfo()
        nprow = int(np.sqrt(1.0*nprocs))
        npcol = int(nprocs/nprow)

        context = pys.blacs_get(-1)
        pys.blacs_gridinit(context, nprow, npcol)

        nprow, npcol, myrow, mycol = pys.blacs_gridinfo(context)

        # initialize scattered matrix
        myArows = pys.numroc(A.shape[0], nb, myrow, 0, nprow)
        myAcols = pys.numroc(A.shape[1], nb, mycol, 0, npcol)

        ath = scatter_matrix(A, nprow, npcol, myrow, mycol, nb,
                        myArows, myAcols, order="F")
        desca, info = pys.descinit(N, N, nb, nb, context, myArows)

        pys.blacs_exit()

    """

    scatter = np.zeros((myrows, mycols), dtype=full.dtype, order=order)

    for i in range(full.shape[0]):
        iproc, myi = g2l(i, full.shape[0], nprow, nb)
        if row == iproc:
            for j in range(full.shape[1]):
                jproc, myj = g2l(j, full.shape[1], npcol, nb)
                if col == jproc:
                    scatter[myi, myj] = full[i, j]

    return scatter

def get_matrix_index(mat_dim, nprow, npcol, row, col, nb, myrows, mycols, dtype=int):
    """
        store the conversion from local to global index to gather the final matrix 
        Input Parameters:
        -----------------
            mat_dim  (1d array): The dimension of the final matrix
            nprow  (int): number of row on the thread grid
            npcol  (int): number of column of the thread grid
            row    (int): row index of the thread on the thread grid
            col    (int): column index of the thread on the thread grid
            nb     (int): number of block
            myrows (int): number of row of the scattered matrix
            mycols (int): number of column of the scattered matrix
            dtype (numpy dtype, optional): int of the index araay, must be integer kind

        Output Parameters:
        ------------------
            global_index (3d array): conversion array between local and global index

        Example:
        --------

        import numpy as np
        import pyscalapack.pyscalapack as pys
        from pyscalapack.tools.m_utils import scatter_matrix

        # input
        N = 10
        A = np.empty(N, N) ! 

        # Initialize the grid
        iam, nprocs = pys.blacs_pinfo()
        nprow = int(np.sqrt(1.0*nprocs))
        npcol = int(nprocs/nprow)

        context = pys.blacs_get(-1)
        pys.blacs_gridinit(context, nprow, npcol)

        nprow, npcol, myrow, mycol = pys.blacs_gridinfo(context)

        # initialize scattered matrix
        myArows = pys.numroc(A.shape[0], nb, myrow, 0, nprow)
        myAcols = pys.numroc(A.shape[1], nb, mycol, 0, npcol)

        gb = get_matrix_index(A.shape, nprow, npcol, myrow, mycol, nb,
                        myArows, myAcols, order="F")

        pys.blacs_exit()

    """


    global_index = np.zeros((myrows, mycols, 2), dtype=dtype)
    for i in range(mat_dim[0]):
        iproc, myi = g2l(i, mat_dim[0], nprow, nb)
        if row == iproc:
            for j in range(mat_dim[1]):
                jproc, myj = g2l(j, mat_dim[1], npcol, nb)
                if col == jproc:
                    global_index[myi, myj, 0] = i
                    global_index[myi, myj, 1] = j

    return global_index

def get_vector_index(N, nprow, row, nb, myrows, dtype=int):
    """

        N   (int): dimension full vector
    """
    
    global_index = np.zeros((myrows), dtype=dtype)
    for i in range(N):
        iproc, myi = g2l(i, N, nprow, nb)
        if row == iproc:
            global_index[myi] = i

    return global_index


def gather_matrix(cth, C_dim, myCrows, myCcols, comm, index_dist):
    """
        Gather the scattered matrix into the master node
        
        Input Parameters:
        -----------------
            cth         (2d array): the scattered matrix to gather
            C_dim       (1d array): the dimension of the gathered matrix
            myCrows     (int): number of row of the scattered matrix
            myCcols     (int): number of column of the scattered matrix
            comm        (MPI communicator)
            index_dist:
                    * Master node: (4d array: (nprocs, myCrows, myCcols, 2)): conversion array
                                between local and global index
                    * Other nodes: None

        Ouput Parameters:
        -----------------
            C_mpi:
                * Master node (2d array): Gathered matrix of dimension C_dim
                * Other node: None

        Example:
        --------
            see example/test_gemm.py
    """

    iam, nprocs = pys.blacs_pinfo()
    
    C_mpi = None
    if iam == 0:
        C_mpi = np.zeros((C_dim[0], C_dim[1]), dtype=cth.dtype)
        for proc in range(nprocs):
            for i in range(myCrows):
                for j in range(myCcols):
                    if proc == 0:
                        C_mpi[index_dist[0, i, j, 0], index_dist[0, i, j, 1]] = \
                                    cth[i, j]
                    else:
                        C_mpi[index_dist[proc, i, j, 0], index_dist[proc, i, j, 1]] = \
                                    comm.recv(source=proc)
    else:
        for i in range(myCrows):
            for j in range(myCcols): 
                comm.send(cth[i, j], dest=0)

    return C_mpi
