! Shorthand notations
!
! <tchar=s,d,cs,zd>
! <tchar2c=cs,zd>
!
! <prefix2=s,d>
! <prefix2c=c,z>
! <prefix3=s,sc>
! <prefix4=d,dz>
! <prefix6=s,d,c,z,c,z>
!
! <ftype2=real,double precision>
! <ftype2c=complex,double complex>
! <ftype3=real,complex>
! <ftype4=double precision,double complex>
! <ftypereal3=real,real>
! <ftypereal4=double precision,double precision>
! <ftype6=real,double precision,complex,double complex,\2,\3>
! <ftype6creal=real,double precision,complex,double complex,\0,\1>
!
! <ctype2=float,double>
! <ctype2c=complex_float,complex_double>
! <ctype3=float,complex_float>
! <ctype4=double,complex_double>
! <ctypereal3=float,float>
! <ctypereal4=double,double>
! <ctype6=float,double,complex_float,complex_double,\2,\3>
! <ctype6creal=float,double,complex_float,complex_double,\0,\1>

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                         !
!             Tools                       !
!                                         !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine p<prefix>elset( a, ia, ja, desca, alpha )
!    .. Scalar Arguments ..
!    INTEGER            IA, JA
!    <ftype> ALPHA
!    ..
!    .. Array arguments ..
!    INTEGER            DESCA( * )
!    <ftype> A( * )
!    ..
!
! Purpose
! =======
!
! PDELSET sets the distributed matrix entry A( IA, JA ) to ALPHA.
!
! Notes
! =====
!
! Each global data object is described by an associated description
! vector.  This vector stores the information required to establish
! the mapping between an object element and its corresponding process
! and memory location.
!
! Let A be a generic term for any 2D block cyclicly distributed array.
! Such a global array has an associated description vector DESCA.
! In the following comments, the character _ should be read as
! "of the global array".
!
! NOTATION        STORED IN      EXPLANATION
! --------------- -------------- --------------------------------------
! DTYPE_A(global) DESCA( DTYPE_ )The descriptor type.  In this case,
!                                DTYPE_A = 1.
! CTXT_A (global) DESCA( CTXT_ ) The BLACS context handle, indicating
!                                the BLACS process grid A is distribu-
!                                ted over. The context itself is glo-
!                                bal, but the handle (the integer
!                                value) may vary.
! M_A    (global) DESCA( M_ )    The number of rows in the global
!                                array A.
! N_A    (global) DESCA( N_ )    The number of columns in the global
!                                array A.
! MB_A   (global) DESCA( MB_ )   The blocking factor used to distribute
!                                the rows of the array.
! NB_A   (global) DESCA( NB_ )   The blocking factor used to distribute
!                                the columns of the array.
! RSRC_A (global) DESCA( RSRC_ ) The process row over which the first
!                                row of the array A is distributed.
! CSRC_A (global) DESCA( CSRC_ ) The process column over which the
!                                first column of the array A is
!                                distributed.
! LLD_A  (local)  DESCA( LLD_ )  The leading dimension of the local
!                                array.  LLD_A >= MAX(1,LOCr(M_A)).
!
! Let K be the number of rows or columns of a distributed matrix,
! and assume that its process grid has dimension p x q.
! LOCr( K ) denotes the number of elements of K that a process
! would receive if K were distributed over the p processes of its
! process column.
! Similarly, LOCc( K ) denotes the number of elements of K that a
! process would receive if K were distributed over the q processes of
! its process row.
! The values of LOCr() and LOCc() may be determined via a call to the
! ScaLAPACK tool function, NUMROC:
!         LOCr( M ) = NUMROC( M, MB_A, MYROW, RSRC_A, NPROW ),
!         LOCc( N ) = NUMROC( N, NB_A, MYCOL, CSRC_A, NPCOL ).
! An upper bound for these quantities may be computed by:
!         LOCr( M ) <= ceil( ceil(M/MB_A)/NPROW )*MB_A
!         LOCc( N ) <= ceil( ceil(N/NB_A)/NPCOL )*NB_A
!
! Arguments
! =========
!
! A       (local output) <ftype> pointer into the local memory
!         to an array of dimension (LLD_A,*) containing the local
!         pieces of the distributed matrix A.
!
! IA      (global input) INTEGER
!         The row index in the global array A indicating the first
!         row of sub( A ).
!
! JA      (global input) INTEGER
!         The column index in the global array A indicating the
!         first column of sub( A ).
!
! DESCA   (global and local input) INTEGER array of dimension DLEN_.
!         The array descriptor for the distributed matrix A.
!
! ALPHA   (local input) <ftype>
!         The scalar alpha.
! 
! =====================================================================

  callstatement (*f2py_func)( a, &ia, &ja, desca, &alpha )
  callprotoargument <ctype>*, int*, int*, int*, <ctype>*

  integer, intent(in)                 :: ia, ja
  <ftype>, intent(in)                  :: alpha
  integer dimension(*), intent(in)    :: desca
  <ftype> dimension(*), intent(inout)  :: a

end subroutine p<prefix>elset

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                         !
!             Level 1                     !
!                                         !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!




!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                         !
!             Level 2                     !
!                                         !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine p<prefix2>gemv(m, n, alpha, a, ia, ja, desca, x, ix, jx, descx, beta, &
                                y, iy, jy, descy, offx, offy, incx, incy, trans, mth, nth, rows, cols, ly)
  ! p<prefix2>gemv(trans, m, n, alpha, a, ia, ja, desca, x, ix, jx, descx, incx, beta, y, iy, jy, descy, incy)
  ! <prefix2>gemv(trans, m, n, alpha, a, lda, x, incx, beta, y, incy)
  ! Computes a matrix-vector product using a general matrix on different nodes with MPI
  ! 
  ! pgemv(m, n, alpha,a, ia, ja, desca, x, ix, jx, descx, y, iy, jy, descy, beta=0,offx=0,incx=1,offy=0,incy=0,trans=0)
  ! Calculate y <- alpha * op(A) * x + beta * y

  callstatement (*f2py_func)((trans?(trans==2?"C":"T"):"N"),&m,&n,&alpha,a,&ia,&ja,desca, &
       x+offx,&ix,&jx,descx,&incx,&beta,y+offy,&iy,&jy,descy,&incy)
  callprotoargument char*,int*,int*,<ctype2>*,<ctype2>*,int*,int*,int*,<ctype2>*,int*,int*,int*,int*, &
       <ctype2>*,<ctype2>*,int*,int*,int*,int*

  integer optional, intent(in), check(trans>=0 && trans <=2) :: trans = 0
  integer optional, intent(in), check(incx>0||incx<0) :: incx = 1
  integer optional, intent(in), check(incy>0||incy<0) :: incy = 1
  integer, intent(in) :: m, n, ia, ja, ix, jx, iy, jy
  integer intent(in) :: desca(*), descx(*), descy(*)
  <ftype2> intent(in) :: alpha
  <ftype2> intent(in), optional :: beta = 0.0 

  <ftype2> dimension(*), intent(in) :: x
  <ftype2> dimension(ly), intent(in,copy,out), depend(ly),optional :: y
  integer intent(hide), depend(incy,rows,offy) :: ly = &
       (y_capi==Py_None?1+offy+(rows-1)*abs(incy):-1)
  <ftype2> dimension(mth, nth), intent(in) :: a
  integer depend(a), intent(hide):: mth = shape(a,0)
  integer depend(a), intent(hide):: nth = shape(a,1)

  integer optional, intent(in) :: offx=0
  integer optional, intent(in) :: offy=0
  check(offx>=0 && offx<len(x)) :: x
  check(len(x)>offx+(cols-1)*abs(incx)) :: x
  depend(offx,cols,incx) :: x

  check(offy>=0 && offy<len(y)) :: y
  check(len(y)>offy+(rows-1)*abs(incy)) :: y
  depend(offy,rows,incy) :: y

  integer depend(mth,nth,trans), intent(hide) :: rows = (trans?nth:mth)
  integer depend(mth,nth,trans), intent(hide) :: cols = (trans?mth:nth)

end subroutine p<prefix2>gemv


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                         !
!             Level 3                     !
!                                         !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine p<prefix2>gemm(m,n,k,alpha,a,ia,ja,desca,b,ib,jb,descb,beta, &
                         c,ic,jc,descc,trans_a,trans_b,lda,ka,ldb,kb,mth,nth,kth)
  ! Computes a scalar-matrix-matrix product and adds the result to a
  ! scalar-matrix product.
  !
  ! c = gemm(alpha,a,b,beta=0,c=0,trans_a=0,trans_b=0,overwrite_c=0)
  ! Calculate C <- alpha * op(A) * op(B) + beta * C
  ! m, n, k must be provided, they are the dimension of the full matrix

  callstatement (*f2py_func)((trans_a?(trans_a==2?"C":"T"):"N"), &
       (trans_b?(trans_b==2?"C":"T"):"N"),&m,&n,&k,&alpha,a,&ia,&ja,desca, &
       b,&ib,&jb,descb,&beta,c,&ic,&jc,descc)
  callprotoargument char*,char*,int*,int*,int*,<ctype2>*,<ctype2>*,int*,int*,int*, &
       <ctype2>*,int*,int*,int*,<ctype2>*,<ctype2>*,int*,int*,int*

  integer optional,intent(in),check(trans_a>=0 && trans_a <=2) :: trans_a = 0
  integer optional,intent(in),check(trans_b>=0 && trans_b <=2) :: trans_b = 0
  <ftype2> intent(in) :: alpha
  integer intent(in) :: m, n, k, ia,ja,ib,jb,ic,jc
  integer intent(in) :: desca(*), descb(*), descc(*)
  <ftype2> intent(in),optional :: beta = 0.0 

  <ftype2> dimension(lda, ka),intent(in) :: a
  <ftype2> dimension(ldb, kb),intent(in) :: b
  <ftype2> dimension(mth,nth),intent(in,out,copy),depend(mth,nth),optional :: c
  check(shape(c,0)==mth && shape(c,1)==nth) :: c
  
  integer depend(a), intent(hide) :: lda = shape(a,0)
  integer depend(a), intent(hide) :: ka = shape(a,1)
  integer depend(b), intent(hide) :: ldb = shape(b,0)
  integer depend(b), intent(hide) :: kb = shape(b,1)

  integer depend(a,trans_a,ka,lda),intent(hide):: mth = (trans_a?ka:lda)
  integer depend(a,trans_a,ka,lda),intent(hide):: kth = (trans_a?lda:ka)
  integer depend(b,trans_b,kb,ldb,kth),intent(hide),check(trans_b?kb==kth:ldb==kth) :: &
       nth = (trans_b?ldb:kb)

end subroutine p<prefix2>gemm
