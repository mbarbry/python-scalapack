from __future__ import division

lib_info = {'libraries': ["blas", "blacsCinit-openmpi","blacs-openmpi", "scalapack-openmpi"],
            'library_dirs': ["/usr/lib"],
            'define_macros': [], 
            'include_dirs': ["/usr/include"]
        }

compilers = {"CC": "mpicc",
             "FC": "mpif90",
             "CXX": "mpic++"}
