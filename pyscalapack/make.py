from __future__ import division
import os
import argparse

def generate_src_file(fname, replace, outdir="."):
    """
    generate pyf file from pyf.src
    The dictionary replace can contain
        <type>: [list of types (real(4), complex(4) ... )]
        <prefix>: [list of prefix coresponding to type (s, c, ....)]
        <ctype>: [list of ctype coresponding to type (float, float_complex)]
    """
    inName = fname + ".src"
    with open(inName, "r") as in_file:
        template = in_file.read()
 
    outdir += "/"
    f = open(outdir + fname, "w")
    if isinstance(replace, dict):
        if len(replace) > 0:
            keys = list(replace.keys())
            for iv in range(len(replace[keys[0]])):
                new = template
                for k, v in replace.items():
                    new = new.replace(k, v[iv])
                f.write(new + "\n")
        else:
            f.write(template)
    else:
        raise ValueError("replace must be a dictionary!!")
    f.close()

def build(compilers="gnu", lib = "", scalapack_lib=None, f2py_build_dir=None):
    if compilers == "gnu":
        os.system("export CC=mpicc")
        os.system("export FC=mpif90")
        os.system("export CXX=mpic++")
        if scalapack_lib is None:
            scalapack_lib = "-lblas -lblacsCinit-openmpi -lblacs-openmpi -lscalapack-openmpi"
    elif compilers == "intel":
        os.system("export CC=mpiicc")
        os.system("export FC=mpiifort")
        os.system("export CXX=mpiicpc")
        if scalapack_lib is None:
            scalapack_lib = "-lmkl_scalapack_lp64 -lmkl_blacs_intelmpi_lp64 -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -liomp5 -lpthread -lm"
    else:
        raise ValueError("only gnu or intel compilers supported")

    try:
        os.mkdir("build")
    except:
        os.system("rm build/*")

    # Compile blacs wrapper
    # os.system("make -f makefile." + compilers)

    # Compile pblas and scalapack wrappers
    fnames = {"pyscalapack.pyf": {".src": [""]},
              "m_scalapack_tools.pyf": {},
              "m_blacs.pyf": {},
              "m_pblas.pyf": {"<type>": ["real(4)", "real(8)"],
                              "<prefix>": ["s", "d"],
                              "<ctype>": ["float", "double"]},
              "m_scalapack.pyf": {"<type>": ["real(4)", "real(8)"],
                                  "<prefix>": ["s", "d"],
                                  "<ctype>": ["float", "double"]},
    }

    for fn, replace in fnames.items():
        generate_src_file(fn, replace, outdir="build")

    main = "pyscalapack.pyf"
    lib += scalapack_lib 
    macro = "-DMPI"

    if f2py_build_dir is not None:
        macro += " --verbose --build-dir " + f2py_build_dir 

    os.chdir("build")
    os.system("f2py -c " + main + " " + macro +" " + lib)
    os.system("cp *.so ../../")

def clean():
    os.system("rm -r build")
    os.system("rm ../*.so")
    


parser = argparse.ArgumentParser()
parser.add_argument("action", type=str, help="action to perform: build or clean")

args_par = parser.parse_args()

if args_par.action == "build":
    build(f2py_build_dir="/home/marc/temp/build-f2py")
elif args_par.action == "clean":
    clean()
else:
    raise ValueError("action can be only build or clean")
