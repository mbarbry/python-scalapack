"""
    Compute the eigenvalues and eigenvectors of a real generalized symmetric-definite
    eigenproblem of the form 
        A*x = lambda*B*x

    Test the mpi implementation
"""

from mpi4py import MPI
import numpy as np
from scipy.linalg.lapack import dsygv
import pyscalapack.pyscalapack as pys
import sys


def sygv_mpi(A_mpi, B_mpi, N, nb, order="F"):
    # initialise mpi
    iam, nprocs = pys.blacs_pinfo()
    print("iam = {0}, nprocs =  {1}".format(iam, nprocs))

    nprow = int(np.sqrt(1.0*nprocs))
    npcol = int(nprocs/nprow)
    print("nprow, npcol = ", nprow, npcol)

    context = pys.blacs_get(-1)
    pys.blacs_gridinit(context, nprow, npcol)

    nprow, npcol, myrow, mycol = pys.blacs_gridinfo(context)
    print("nprow, npcol, myrow, mycol = ", nprow, npcol, myrow, mycol)

    if (myrow < 0 or mycol < 0):
        raise ValueError("myrow < 0 or mycol < 0")

    lda = pys.numroc(N, nb, myrow, 0, nprow)
    print("lda = ", lda)

    ath = np.zeros((lda, lda), dtype=np.float64, order=order)
    bth = np.zeros((lda, lda), dtype=np.float64, order=order)
    zth = np.zeros((lda, lda), dtype=np.float64, order=order)

    #desca = np.zeros((50), dtype = np.int32, order=order)
    #descb = np.zeros((50), dtype = np.int32, order=order)
    #descz = np.zeros((50), dtype = np.int32, order=order)

    desca, info = pys.descinit(N, N, nb, nb, context, lda)
    descb, info = pys.descinit(N, N, nb, nb, context, lda)
    descz, info = pys.descinit(N, N, nb, nb, context, lda)

    comm.Barrier()

    for i in range(N):
        for j in range(N):
            pys.pdelset(ath, i+1, j+1, desca, A_mpi[i, j])
            pys.pdelset(bth, i+1, j+1, descb, B_mpi[i, j])

    comm.Barrier()

    a, m, nz, w, z, work, iwork, info = \
            pys.pdsygvx(nprow, npcol, N, ath, 1, 1, desca, bth, 1, 1, descb, 1, 1, descz)

    print(work, iwork, info)
    lwork = int(work[0])
    liwork = iwork[0]
    a, m, nz, w, z, work, iwork, info = \
            pys.pdsygvx(nprow, npcol, N, ath, 1, 1, desca, bth, 1, 1, descb, 1, 1, descz,
                    lwork=lwork, liwork=liwork)
    print("w = ", w)
    pys.blacs_exit()

    return w

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
print(rank)


N = 4       # size of the problem (4x4 matrix)
nb = 1      # block size
order = "F"

A_mpi = np.zeros((N, N), dtype=np.float64, order=order)
B_mpi = np.eye(N, dtype=np.float64)
ws = np.zeros(N, dtype=np.float64, order=order)

# Initialize data
if rank == 0:
    for i in range(N):
        for j in range(N):
            A_mpi[i, j] = (i-0.5*j)*np.random.randn(1)[0]

    A_mpi += A_mpi.T

    As = np.copy(A_mpi)
    Bs = np.copy(B_mpi)

    vs, ws, info = dsygv(As, Bs, uplo="U")
    print("ws = ", ws)
    print(info)
# End solving serial problem

comm.Bcast(A_mpi)
comm.Bcast(B_mpi)
comm.Bcast(ws)

comm.Barrier()
# Same diagonalization with Scalapack ... 
w_mpi = sygv_mpi(A_mpi, B_mpi, N, nb, order="F")

print("Error = ", np.sum(abs(w_mpi - ws)))
