Python wrapper for ScaLapack
============================

Installation
------------

* Prerequisites
    - Python
    - Blacs, ScaLapack
    - mpi4py (pip install mpi4py)

* Installation with system python

        git clone https://gitlab.com/mbarbry/python-scalapack.git
        cd python-scalapack
        cp pyscalapack/customize.py.arch pyscalapack/customize.py  # (you may need to edit it)
        python setup.py install
        
* Installation with Python Anaconda 

If you are using a python version from [Anaconda](https://www.anaconda.com/) distribution, then it comes already with it own mpi library and mpi4py is already compile with
it. You can try to install ScaLapack using

        conda install -c conda-forge scalapack 
        git clone https://gitlab.com/mbarbry/python-scalapack.git
        cd python-scalapack
        cp pyscalapack/customize.py.arch pyscalapack/customize.py  # (you may need to edit it)
        python setup.py install

If there is no other mpi/Scalapack library installed on your system this may work. Otherwise you will probably run into conflict with the library from your system. If it this is the case,
then the best way to install pyscalapack is to create a virtual environment to avoid any conflicts between libraries. To install pyscalapack within a virtual environment use the following steps,

Create the evironment (read this [tutorial](https://uoa-eresearch.github.io/eresearch-cookbook/recipe/2014/11/20/conda/) for more informations),

    conda create -n pysca python=3.6 anaconda
    source activate pysca
    
Ensure you are using the right mpicc,

    mpicc --version
    
Install mpi4py with your system mpi,

    wget https://bitbucket.org/mpi4py/mpi4py/downloads/mpi4py-2.0.0.tar.gz
    tar -xvzf mpi4py-2.0.0.tar.gz
    cd mpi4py-2.0.0/
    python setup.py build
    python setup.py install
    cd ..

Finally install pyscalapack using your system mpi/scalapack libraries,

    git clone https://gitlab.com/mbarbry/python-scalapack.git
    cd python-scalapack
    cp pyscalapack/customize.py.anaconda.scalapack pyscalapack/customize.py

Edit the customize.py file providing the right path to mpi and scalapack libraries

    python setup.py build
    python setup.py install

Try to import the library,

    cd
    python
    import pyscalapack.pyscalapack


There are a few customize.py.arch examples. You will need to edit it as function of your system. 
This file consist of two dictionaries "lib_info" and "compilers" which define the include directories
and libraries and compilers, correspondingly. For example,

        lib_info = {'libraries': ["blas", "blacsCinit-openmpi","blacs-openmpi", "scalapack-openmpi"],
                    'library_dirs': ["/usr/lib"],
                    'define_macros': [], 
                    'include_dirs': ["/usr/include"]
                   }

        compilers = {"CC": "mpicc",
                     "FC": "mpif90",
                     "CXX": "mpic++"
                    }

Documentation
-------------
Documentation is available at http://mbarbry.pagekite.me/PyScalapack/

Notes
-----
* Complex version of p?gemv and p?gemm does not compile
* To run tests
      
        cd test
        mpirun -np nprocs python test_??.py

* One needs to be careful with the used mpirun executable. For example, if you use Anaconda Python, 
and you installed PyScalapack in it, then you must use the mpirun coming with anaconda, usually 
$HOME/anaconda/bin/mpirun
Similar precaution applies to Intel Python.

Bug report
----------
Send bugs to Marc Barbry <marc.barbry@mailoo.org>

